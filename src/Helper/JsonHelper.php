<?php

namespace Eternium\Helper;

use Symfony\Component\Console\Helper\Helper;
use Symfony\Component\Console\Output\OutputInterface;

final class JsonHelper extends Helper
{
    public function getName()
    {
        return 'json';
    }

    public function dump(OutputInterface $output, mixed $value): void
    {
        $flags = \JSON_PRETTY_PRINT * (int) $output->isVerbose();
        $output->writeln(\json_encode($this->prepare($value), $flags));
    }

    private function prepare(mixed $value): mixed
    {
        if ($value instanceof \JsonSerializable) {
            $value = $value->jsonSerialize();
        }
        if ($value instanceof \Traversable) {
            $value = \iterator_to_array($value);
        }
        if (\is_array($value)) {
            foreach ($value as &$item) {
                $item = $this->prepare($item);
            }
        }

        return $value;
    }
}
