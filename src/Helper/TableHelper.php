<?php

namespace Eternium\Helper;

use Symfony\Component\Console\Helper\Helper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

final class TableHelper extends Helper
{
    private const FORMAT_NULL = '<fg=gray>N/A</>';

    public function getName()
    {
        return 'table';
    }

    public function dump(OutputInterface $output, array $header, iterable $rows): void
    {
        $table = new Table($output);
        $table->setStyle('box');
        $table->setHeaders($header);
        foreach ($rows as $row) {
            $table->addRow($row);
        }
        $table->render();
    }

    public function formatIfNull(?string $value): string
    {
        return $value ?? self::FORMAT_NULL;
    }

    public function formatNumber(float $num, int $decimals = 0): string
    {
        return \number_format($num, $decimals, '.', '');
    }

    public function formatPlatform(string $os): string
    {
        return match ($os) {
            '', 'default' => self::FORMAT_NULL,
            'ios' => 'iOS',
            default => \ucfirst($os),
        };
    }
}
