<?php

namespace Eternium\Command;

use Eternium\Id;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LeaderboardsCommand extends BaseCommand
{
    protected static $defaultName = 'leaderboards';

    protected function configure(): void
    {
        $this->setAliases(['lb']);
        $this->setDescription('List leaderboards');
        $this->addArgument('leaderboard', InputArgument::OPTIONAL, 'The leaderboard ID or name');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $leaderboard = (string) $input->getArgument('leaderboard');
        $id = Id::tryParse($leaderboard);
        if (null === $id) {
            $leaderboards = $this->filterByName($this->fetch('leaderboards'), (string) $leaderboard);
        } else {
            $leaderboards = [$this->fetch("leaderboards/{$id}")];
        }

        if ($input->getOption('json')) {
            $this->getHelper('json')->dump($output, $leaderboards);
        } else {
            $this->getHelper('table')->dump($output, [
                'ID',
                'Name',
                'ScoreTypes',
                'Description',
                'Entries',
                'SubLeaderboard',
            ], $this->makeRows($leaderboards));
        }

        return self::SUCCESS;
    }

    private function makeRows(iterable $leaderboards): iterable
    {
        foreach ($leaderboards as $lb) {
            $row = [
                $lb['id'],
                $lb['name'],
                join(', ', $lb['scoreTypes']),
                $this->getHelper('table')->formatIfNull($lb['description'] ?? null),
            ];

            yield [...$row, (int) $lb['entriesCount'], ''];
            foreach ($lb['subLeaderboards'] ?? [] as $slb) {
                yield [...$row, (int) $slb['entriesCount'], $slb['name']];
            }
        }
    }

    private function filterByName(iterable $leaderboards, string $name): iterable
    {
        foreach ($leaderboards as $lb) {
            if (false !== \stripos($lb['name'], $name)) {
                yield $lb;
            }
        }
    }
}
