<?php

namespace Eternium\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NewsCommand extends BaseCommand
{
    protected static $defaultName = 'news';

    protected function configure(): void
    {
        $this->setDescription('List news');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $data = $this->fetch('getNews');
        if ($input->getOption('json')) {
            $this->getHelper('json')->dump($output, $data);

            return self::SUCCESS;
        }

        throw new \LogicException('Not implemented yet');

        return self::SUCCESS;
    }
}
