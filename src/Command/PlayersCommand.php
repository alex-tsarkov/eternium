<?php

namespace Eternium\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PlayersCommand extends BaseCommand
{
    protected static $defaultName = 'players';

    protected function configure(): void
    {
        $this->setDescription('List players');
        $this->addArgument('query', InputArgument::REQUIRED, 'The player email, username or GUID');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $query = $input->getArgument('query');
        $options = match (true) {
            \str_starts_with($query, 'MMID-') => ['provider' => 'legacy', 'principal' => $query],
            \str_contains($query, '@') => ['provider' => 'email', 'principal' => $query],
            default => ['username' => $query],
        };

        $players = $this->fetch('players', $options);

        if ($input->getOption('json')) {
            $this->getHelper('json')->dump($output, $players);
        } else {
            $this->getHelper('table')->dump($output, [
                'ID',
                'Username',
                'Email',
                'GUID',
            ], $this->makeRows($players));
        }

        return self::SUCCESS;
    }

    private function makeRows(iterable $players): iterable
    {
        $table = $this->getHelper('table');
        foreach ($players as $player) {
            $profiles = \array_column($player['profiles'], 'principal', 'provider');

            yield [
                $player['id'],
                $table->formatIfNull($player['username'] ?? null),
                $table->formatIfNull($profiles['email'] ?? null),
                $table->formatIfNull($profiles['legacy'] ?? null),
            ];
        }
    }
}
