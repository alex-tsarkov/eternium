<?php

namespace Eternium\Command;

use Eternium\Id;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RankingsCommand extends BaseCommand
{
    protected static $defaultName = 'rankings';

    protected function configure(): void
    {
        $this->setAliases(['rank']);
        $this->setDescription('List the leaderboard entries');
        $this->addArgument('leaderboard', InputArgument::REQUIRED, 'The leaderboard ID');
        $this->addArgument('ranking', InputArgument::OPTIONAL, 'The ranking position');
        $this->addOption('class', 'c', InputOption::VALUE_REQUIRED, 'Show sub leaderboard entries', '');
        $this->addOption('page', 'p', InputOption::VALUE_REQUIRED, 'Go to page', '1');
        $this->addOption('page-size', 's', InputOption::VALUE_REQUIRED, 'Set page size', '25');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $input->setArgument('leaderboard', Id::parse($input->getArgument('leaderboard')));

        $ranking = $input->getArgument('ranking');
        if (null !== $ranking) {
            $ranking = (int) $ranking;
            $ranking = $ranking > 0 ? $ranking : throw new \UnexpectedValueException('Ranking exceeds the specified range');
            $input->setArgument('ranking', $ranking);
        }

        $input->setOption('class', \strtoupper(\strtr($input->getOption('class'), ' ', '_')));
        $input->setOption('page', (int) $input->getOption('page'));
        $input->setOption('page-size', (int) $input->getOption('page-size'));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = $input->getArgument('leaderboard');
        $ranking = $input->getArgument('ranking');
        $options = [];
        if ($input->hasOption('class')) {
            $options['subleaderboard'] = $input->getOption('class');
        }

        $leaderboard = $this->fetch("leaderboards/{$id}");
        if (!$input->getOption('json') && $output->isVerbose()) {
            $output->writeln("Listing <info>{$leaderboard['name']}</info>, entries: <info>{$leaderboard['entriesCount']}</info>");
        }

        $entries = match ($ranking) {
            null => $this->fetch("leaderboards/{$id}/rankings", [
                'page' => $input->getOption('page'),
                'pageSize' => $input->getOption('page-size'),
            ] + $options),
            default => [$this->fetch("leaderboards/{$id}/rankings/{$ranking}", $options)],
        };

        if ($input->getOption('json')) {
            $this->getHelper('json')->dump($output, $entries);
        } elseif (['INCREMENTAL'] === $leaderboard['scoreTypes']) {
            $this->getHelper('table')->dump($output, [
                '#',
                'Name',
                'Class',
                'CL',
                'AvgIL',
                'Rating',
            ], $this->makeArenaRows($entries));
        } else {
            $this->getHelper('table')->dump($output, [
                '#',
                'Name',
                'Platform',
                'Class',
                'CL',
                'AvgIL',
                'TL',
                'Time',
                'Deaths',
            ], $this->makeTrialRows($entries));
        }

        return self::SUCCESS;
    }

    private function makeArenaRows(iterable $entries): iterable
    {
        $table = $this->getHelper('table');

        foreach ($entries as $entry) {
            $hero = $entry['payload']['hero'];

            yield [
                (int) $entry['ranking'],
                $this->formatName($entry['payload']['name'], $hero['selectedPlayerNameID'] ?? ''),
                $hero['class'],
                (int) $entry['payload']['champion_level'],
                $table->formatNumber($this->getAverageItemLevel($hero['equipped']), 1),
                (int) $entry['score'],
            ];
        }
    }

    private function makeTrialRows(array $entries): iterable
    {
        $table = $this->getHelper('table');

        foreach ($entries as $entry) {
            $hero = $entry['payload']['hero'];
            $stats = $entry['payload']['trialStats'];

            yield [
                (int) $entry['ranking'],
                $this->formatName($entry['payload']['name'], $hero['selectedPlayerNameID'] ?? ''),
                $table->formatPlatform($entry['payload']['devInfo']['platform'] ?? ''),
                $hero['class'],
                (int) $entry['payload']['champion_level'],
                $table->formatNumber($this->getAverageItemLevel($hero['equipped']), 1),
                $this->getTrialLevel($entry['score']),
                $this->formatTime($this->getTrialTime((int) $entry['score'])),
                (int) $stats['heroDeaths'],
            ];
        }
    }

    private function formatName(string $name, string $title = ''): string
    {
        \assert('' !== $name);

        if ('' !== $title) {
            $title = \ucwords(\strtr($title, '_', ' '));
            $name = "<comment>{$title}</comment> {$name}";
        }

        return $name;
    }

    private function formatTime(int $time): string
    {
        \assert($time >= 0);

        return \sprintf('%d:%02d', $time / 60, $time % 60);
    }

    private function getAverageItemLevel(array $items): float
    {
        $levels = \array_column($items, 'itemLevel');
        if (!$levels) {
            return 0.0;
        }

        return \array_sum($levels) / \count($levels);
    }

    private function getTrialLevel(int $score): int
    {
        \assert($score >= 10000);

        return (int) ($score / 10000);
    }

    private function getTrialTime(int $score): int
    {
        \assert($score >= 10000);

        return 9999 - $score % 10000;
    }
}
