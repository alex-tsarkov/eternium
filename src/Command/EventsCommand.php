<?php

declare(strict_types=1);

namespace Eternium\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EventsCommand extends BaseCommand
{
    protected static $defaultName = 'events';

    protected function configure(): void
    {
        $this->setDescription('List game events');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $data = $this->fetch('getGameEvents');
        if ($input->getOption('json')) {
            $this->getHelper('json')->dump($output, $data);
        } else {
            $this->getHelper('table')->dump($output, [
                'Title',
                'StartDate',
                'EndDate',
            ], $this->makeRows($data['events']));
        }

        return self::SUCCESS;
    }

    private function makeRows(iterable $events): iterable
    {
        foreach ($events as $event) {
            yield [
                $event['title'],
                $this->formatDate($event['start_date']),
                $this->formatDate($event['end_date']),
            ];
        }
    }

    private function formatDate(string $time): string
    {
        return \DateTimeImmutable::createFromFormat('U', \substr($time, 0, -3))->format('Y-m-d H:i');
    }
}
