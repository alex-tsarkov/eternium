<?php

declare(strict_types=1);

namespace Eternium\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StatusCommand extends BaseCommand
{
    protected static $defaultName = 'status';

    protected function configure(): void
    {
        $this->setDescription('Get system status');
        $this->addOption('os', '', InputOption::VALUE_REQUIRED, 'Display status for the given platform', 'steam');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $os = $input->getOption('os');
        $input->setOption('os', match (\strtolower((string) $os)) {
            'ios' => 14,
            'android' => 18,
            'windows' => 31,
            'amazon' => 34,
            (string) (int) $os => (int) $os,
            default => 28, // steam
        });
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $options = [];
        if ($input->hasOption('os')) {
            $options['OS'] = $input->getOption('os');
        }

        $data = $this->fetch('getSystemStatus', $options);
        if ($input->getOption('json')) {
            $this->getHelper('json')->dump($output, $data);
        } else {
            $this->getHelper('table')->dump($output, [
                'Version',
                'Platform',
                'Status',
            ], [[
                $data['status']['version'],
                $this->getHelper('table')->formatPlatform($data['status']['platform']),
                $this->formatStatus($data['status']['message'], 0 === $data['status']['code']),
            ]]);
        }

        return self::SUCCESS;
    }

    private function formatStatus(string $status, bool $ok): string
    {
        $status = \rtrim($status);
        if (!$ok) {
            $status = "<fg=red>{$status}</>";
        }

        return $status;
    }
}
