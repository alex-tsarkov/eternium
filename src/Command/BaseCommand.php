<?php

namespace Eternium\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class BaseCommand extends Command
{
    public function __construct(
        private HttpClientInterface $httpClient,
        ?string $name = null,
    ) {
        parent::__construct($name);
    }

    protected function fetch(string $uri, array $options = []): array
    {
        return $this->httpClient->request('GET', $uri, [
            'query' => $options,
        ])->toArray();
    }
}
