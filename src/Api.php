<?php

namespace Eternium;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class Api
{
    private static HttpClientInterface $mfp;
    private static HttpClientInterface $v2;
    private static HttpClientInterface $v3;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function mfp(): HttpClientInterface
    {
        return self::$mfp ??= HttpClient::createForBaseUri('https://eternium-mfp.alex-tsarkov.workers.dev/api/');
    }

    public static function v2(): HttpClientInterface
    {
        return self::$v2 ??= HttpClient::createForBaseUri('https://eternium.alex-tsarkov.workers.dev/api/v2/');
    }

    public static function v3(): HttpClientInterface
    {
        return self::$v3 ??= HttpClient::createForBaseUri('https://eternium.alex-tsarkov.workers.dev/api/v3/');
    }
}
