<?php

namespace Eternium;

final class Id implements \Stringable
{
    public const LENGTH = 24;

    private string $id;

    private function __construct(string $id)
    {
        $this->id = \strtolower($id);
    }

    public function __toString(): string
    {
        return $this->id;
    }

    public static function isValid(string $str): bool
    {
        return self::LENGTH === \strlen($str) && \ctype_xdigit($str);
    }

    public static function parse(string $str): self
    {
        return self::isValid($str) ? new self($str) : throw new \UnexpectedValueException('Invalid value for ID');
    }

    public static function tryParse(string $str): ?self
    {
        return self::isValid($str) ? new self($str) : null;
    }
}
