<?php

declare(strict_types=1);

namespace Eternium;

use Eternium\Command\EventsCommand;
use Eternium\Command\LeaderboardsCommand;
use Eternium\Command\NewsCommand;
use Eternium\Command\PlayersCommand;
use Eternium\Command\RankingsCommand;
use Eternium\Command\StatusCommand;
use Eternium\Helper\JsonHelper;
use Eternium\Helper\TableHelper;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Input\InputOption;

final class Application extends ConsoleApplication
{
    public function __construct(string $name = 'Eternium')
    {
        parent::__construct($name, \Composer\InstalledVersions::getRootPackage()['pretty_version']);

        $this->getHelperSet()->set(new JsonHelper());
        $this->getHelperSet()->set(new TableHelper());
        $this->getDefinition()->addOption(new InputOption('json', '', InputOption::VALUE_NONE, 'Display raw response'));

        $this->addCommands([
            new EventsCommand(Api::v3()),
            new LeaderboardsCommand(Api::mfp()),
            new NewsCommand(Api::v2()),
            new PlayersCommand(Api::mfp()),
            new RankingsCommand(Api::mfp()),
            new StatusCommand(Api::v2()),
        ]);
    }
}
