<?php

return (new \PhpCsFixer\Config())
    ->setRules([
        '@PhpCsFixer' => true,
    ])
    ->setFinder(
        (new \PhpCsFixer\Finder())
            ->files()
            ->in('src')
            ->append(['.php-cs-fixer.dist.php', 'bin/eternium'])
    )
;
